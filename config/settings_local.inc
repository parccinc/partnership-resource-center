<?php
/**
 * Integration settings for local dev environment.
 */

// bt_onelogin_saml

// Service provider
/*
It probably isn't practical to add all of our local environments as Service
Providers in SAML.  So for the time being, if you want to use SSO locally
you'll need to configure your instance to use http://parcc-prc.dd:8083. Easiest
way to do this is probably using Dev Desktop. Sorry!
*/
$conf['bt_onelogin_saml_sp_entityid'] = 'http://parcc-prc.dd:8083';
$conf['bt_onelogin_saml_sp_acs_url'] = 'http://parcc-prc.dd:8083/onelogin_saml/acs';
$conf['bt_onelogin_saml_sp_sls_url'] = 'http://parcc-prc.dd:8083/user/logout';

// Identity provider
$conf['bt_onelogin_saml_idp_entityid'] = 'https://testidp.parcconline.org/simplesaml/saml2/idp/metadata.php';
$conf['bt_onelogin_saml_idp_sls_url'] = 'https://testidp.parcconline.org/simplesaml/saml2/idp/SingleLogoutService.php';
$conf['bt_onelogin_saml_idp_sso_url'] = 'https://testidp.parcconline.org/simplesaml/saml2/idp/SSOService.php';
$conf['bt_onelogin_saml_idp_x509cert'] = 'MIIGaDCCBVCgAwIBAgIITqzsiaP6uZ0wDQYJKoZIhvcNAQELBQAwgbQxCzAJBgNVBAYTAlVTMRAwDgYDVQQIEwdBcml6b25hMRMwEQYDVQQHEwpTY290dHNkYWxlMRowGAYDVQQKExFHb0RhZGR5LmNvbSwgSW5jLjEtMCsGA1UECxMkaHR0cDovL2NlcnRzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkvMTMwMQYDVQQDEypHbyBEYWRkeSBTZWN1cmUgQ2VydGlmaWNhdGUgQXV0aG9yaXR5IC0gRzIwHhcNMTUwOTE1MTI1NTM4WhcNMTYwOTE1MTIxMDM4WjBBMSEwHwYDVQQLExhEb21haW4gQ29udHJvbCBWYWxpZGF0ZWQxHDAaBgNVBAMTE3Nzby5wYXJjY29ubGluZS5vcmcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDdZjS9RvcyaRdc2syuAV5TDyRkgqV1npLp9CmatodoY92nT3VHMLxteWqw/fSX0dQaBrFVa3ELBE1XulPHjE9tEhMGuajpSWFE0NKyeXIgFpGUGsK4tWAgRr7U0HJJOC2Jeo939+2RogxsCJ0eOUWwDnd6xZocmGNlYjgvbdmvbw9cNsG9e0GNzRwaH/68/9xDnDFCVHiT1A10V2NMIKqG1yPvop+8hpR1Fh+0MdcSWtVIBhtG80HQia3+WcjFhCvo20CGWwMZuQAFHCSelX6RUSdvsyOYY6Ymad1R1QY0lVh/k7nnTk9HEmGT5qBhGEu+sp/JPi9wVGmRVHzw+CIvAgMBAAGjggLuMIIC6jAMBgNVHRMBAf8EAjAAMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAOBgNVHQ8BAf8EBAMCBaAwNwYDVR0fBDAwLjAsoCqgKIYmaHR0cDovL2NybC5nb2RhZGR5LmNvbS9nZGlnMnMxLTEyMy5jcmwwUwYDVR0gBEwwSjBIBgtghkgBhv1tAQcXATA5MDcGCCsGAQUFBwIBFitodHRwOi8vY2VydGlmaWNhdGVzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkvMHYGCCsGAQUFBwEBBGowaDAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuZ29kYWRkeS5jb20vMEAGCCsGAQUFBzAChjRodHRwOi8vY2VydGlmaWNhdGVzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkvZ2RpZzIuY3J0MB8GA1UdIwQYMBaAFEDCvSeOzDSDMKIz1/tss/C0LIDOMIIBYwYDVR0RBIIBWjCCAVaCE3Nzby5wYXJjY29ubGluZS5vcmeCF3d3dy5zc28ucGFyY2NvbmxpbmUub3JnghJrMi5wYXJjY29ubGluZS5vcmeCE2Fkcy5wYXJjY29ubGluZS5vcmeCE3ByYy5wYXJjY29ubGluZS5vcmeCE2Fjci5wYXJjY29ubGluZS5vcmeCGHRlc3RsZGFwLnBhcmNjb25saW5lLm9yZ4IUc29sci5wYXJjY29ubGluZS5vcmeCGWZvcm1hdGl2ZS5wYXJjY29ubGluZS5vcmeCF3Byb2RpZHAucGFyY2NvbmxpbmUub3Jnghhwcm9kbGRhcC5wYXJjY29ubGluZS5vcmeCE3RhcC5wYXJjY29ubGluZS5vcmeCE2RjbS5wYXJjY29ubGluZS5vcmeCF3Rlc3RpZHAucGFyY2NvbmxpbmUub3JnghJ0ZC5wYXJjY29ubGluZS5vcmcwHQYDVR0OBBYEFGGKhc8DgZadA/75YlpJ7/4c1amXMA0GCSqGSIb3DQEBCwUAA4IBAQBUk7ZXAKx/ulTfEBytpCLSMrIwkgx00X6RwQwXx3d3Q/p+r3iIi5JSJ0Qy+HcNMPcf6M6MuJhcMdwp7N+7/xR9+QndEFt8J4XM3j9gDJifrVNZ/xCNvZLytJTN1hJZfXELyDnWi+STiOSBY8h24OUI7pC4e2ad6bwZ4uFJ379s/gCo8cPODpNU6PQwC9xF4kyC7bMNpc+9k1P1bZO8xyVRfPQNRbuDufnjGArh9WXetMakEKCFkOO63fqzarGkrk7FZJwIhuPlfax0wRds9onGIxtnIXg82nWBp/qVfCFcKlA5Ou4Rk7sMRXbprruC8aefVJg7aJWDxrTbVI9TUSqE';

// Set the directory for views data export to private, since the tmp directory isn't shared by the different instances
$conf['views_data_export_directory'] = 'private://views-data-export/';

$conf['prc_sso_user_ldap'] = 'LDAP_QA';

$conf['prc_adp_url'] = 'http://int.tds.adp.parcc-ads.breaktech.org';

// Override the default email for sending account upgrade requests
$conf['prc_registration_upgrade_email'] = 'prc-upgrade-account@mailinator.com';