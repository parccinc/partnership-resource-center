<?php
/**
 * @file
 * prc_sso_prod.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function prc_sso_prod_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'autologout_message';
  $strongarm->value = 'Your session is about to expire. Do you want to extend it?';
  $export['autologout_message'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'autologout_padding';
  $strongarm->value = '20';
  $export['autologout_padding'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'autologout_redirect_url';
  $strongarm->value = '<front>';
  $export['autologout_redirect_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'autologout_timeout';
  $strongarm->value = '28800';
  $export['autologout_timeout'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'bt_onelogin_saml_autoprovision';
  $strongarm->value = 1;
  $export['bt_onelogin_saml_autoprovision'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ldap_user_conf';
  $strongarm->value = array(
    'drupalAcctProvisionServer' => 0,
    'ldapEntryProvisionServer' => 'LDAP_Prod',
    'drupalAcctProvisionTriggers' => array(
      2 => '2',
      1 => '1',
    ),
    'ldapEntryProvisionTriggers' => array(
      6 => '6',
      7 => '7',
      8 => '8',
      3 => 0,
    ),
    'orphanedDrupalAcctBehavior' => 'ldap_user_orphan_email',
    'orphanedCheckQty' => '100',
    'userConflictResolve' => 2,
    'manualAccountConflict' => '1',
    'acctCreation' => 4,
    'ldapUserSynchMappings' => array(
      2 => array(
        '[dn]' => array(
          'ldap_attr' => '[dn]',
          'user_attr' => 'user_tokens',
          'convert' => 0,
          'direction' => '2',
          'user_tokens' => 'uid=[property.mail],ou=People,dc=parcconline,dc=org',
          'config_module' => 'ldap_user',
          'prov_module' => 'ldap_user',
          'enabled' => 1,
          'prov_events' => array(
            0 => 4,
            1 => 3,
          ),
        ),
        '[cn]' => array(
          'ldap_attr' => '[cn]',
          'user_attr' => 'user_tokens',
          'convert' => 0,
          'direction' => '2',
          'user_tokens' => '[field.field_first_name] [field.field_last_name]',
          'config_module' => 'ldap_user',
          'prov_module' => 'ldap_user',
          'enabled' => 1,
          'prov_events' => array(
            0 => 4,
            1 => 3,
          ),
        ),
        '[userPassword]' => array(
          'ldap_attr' => '[userPassword]',
          'user_attr' => '[password.user-only]',
          'convert' => 0,
          'direction' => '2',
          'user_tokens' => '',
          'config_module' => 'ldap_user',
          'prov_module' => 'ldap_user',
          'enabled' => 1,
          'prov_events' => array(
            0 => 4,
            1 => 3,
          ),
        ),
        '[givenName]' => array(
          'ldap_attr' => '[givenName]',
          'user_attr' => '[field.field_first_name]',
          'convert' => 0,
          'direction' => '2',
          'user_tokens' => '',
          'config_module' => 'ldap_user',
          'prov_module' => 'ldap_user',
          'enabled' => 1,
          'prov_events' => array(
            0 => 4,
            1 => 3,
          ),
        ),
        '[sn]' => array(
          'ldap_attr' => '[sn]',
          'user_attr' => '[field.field_last_name]',
          'convert' => 0,
          'direction' => '2',
          'user_tokens' => '',
          'config_module' => 'ldap_user',
          'prov_module' => 'ldap_user',
          'enabled' => 1,
          'prov_events' => array(
            0 => 4,
            1 => 3,
          ),
        ),
        '[uid]' => array(
          'ldap_attr' => '[uid]',
          'user_attr' => '[property.mail]',
          'convert' => 0,
          'direction' => '2',
          'user_tokens' => '[field.field_first_name]_[property.uid]',
          'config_module' => 'ldap_user',
          'prov_module' => 'ldap_user',
          'enabled' => 1,
          'prov_events' => array(
            0 => 4,
            1 => 3,
          ),
        ),
        '[mail]' => array(
          'ldap_attr' => '[mail]',
          'user_attr' => '[property.mail]',
          'convert' => 0,
          'direction' => '2',
          'user_tokens' => '',
          'config_module' => 'ldap_user',
          'prov_module' => 'ldap_user',
          'enabled' => 1,
          'prov_events' => array(
            0 => 4,
            1 => 3,
          ),
        ),
        '[objectClass]' => array(
          'ldap_attr' => '[objectClass]',
          'user_attr' => 'user_tokens',
          'convert' => 0,
          'direction' => '2',
          'user_tokens' => 'inetOrgPerson',
          'config_module' => 'ldap_user',
          'prov_module' => 'ldap_user',
          'enabled' => 1,
          'prov_events' => array(
            0 => 4,
            1 => 3,
          ),
        ),
      ),
    ),
    'disableAdminPasswordField' => 0,
  );
  $export['ldap_user_conf'] = $strongarm;

  return $export;
}
