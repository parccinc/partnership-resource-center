<?php
/**
 * @file
 * prc_sso_prod.ldap_query.inc
 */

/**
 * Implements hook_default_ldap_query().
 */
function prc_sso_prod_default_ldap_query() {
  $export = array();

  $qid = new stdClass();
  $qid->disabled = FALSE; /* Edit this to true to make a default qid disabled initially */
  $qid->api_version = 1;
  $qid->qid = 'prc_users_prod';
  $qid->name = 'PRC Users Production';
  $qid->sid = 'LDAP_Prod';
  $qid->status = FALSE;
  $qid->base_dn_str = 'ou=people,dc=parcconline,dc=org';
  $qid->filter = '(cn=*)';
  $qid->attributes_str = '';
  $qid->sizelimit = 0;
  $qid->timelimit = 0;
  $qid->deref = FALSE;
  $qid->scope = TRUE;
  $export['prc_users_prod'] = $qid;

  return $export;
}
