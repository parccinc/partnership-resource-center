<?php
/**
 * @file
 * prc_sso.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function prc_sso_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_formative-instructional-tasks-technology-enhanced:formative-external
  $menu_links['main-menu_formative-instructional-tasks-technology-enhanced:formative-external'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'formative-external',
    'router_path' => 'formative-external',
    'link_title' => 'Formative Instructional Tasks (Technology Enhanced)',
    'options' => array(
      'identifier' => 'main-menu_formative-instructional-tasks-technology-enhanced:formative-external',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_instructional-tools:instructional-tools',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Formative Instructional Tasks (Technology Enhanced)');


  return $menu_links;
}
