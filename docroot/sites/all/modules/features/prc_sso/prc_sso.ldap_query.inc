<?php
/**
 * @file
 * prc_sso.ldap_query.inc
 */

/**
 * Implements hook_default_ldap_query().
 */
function prc_sso_default_ldap_query() {
  $export = array();

  $qid = new stdClass();
  $qid->disabled = FALSE; /* Edit this to true to make a default qid disabled initially */
  $qid->api_version = 1;
  $qid->qid = 'prc_users';
  $qid->name = 'PRC Users';
  $qid->sid = 'LDAP_QA';
  $qid->status = FALSE;
  $qid->base_dn_str = 'ou=people,dc=uat,dc=parcconline,dc=org';
  $qid->filter = '(cn=*)';
  $qid->attributes_str = '';
  $qid->sizelimit = 0;
  $qid->timelimit = 0;
  $qid->deref = FALSE;
  $qid->scope = TRUE;
  $export['prc_users'] = $qid;

  return $export;
}
