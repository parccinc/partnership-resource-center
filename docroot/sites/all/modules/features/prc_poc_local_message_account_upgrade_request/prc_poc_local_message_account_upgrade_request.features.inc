<?php
/**
 * @file
 * prc_poc_local_message_account_upgrade_request.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function prc_poc_local_message_account_upgrade_request_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_message_type().
 */
function prc_poc_local_message_account_upgrade_request_default_message_type() {
  $items = array();
  $items['account_upgrade_request'] = entity_import('message_type', '{
    "name" : "account_upgrade_request",
    "description" : "Account Upgrade Request",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "Upgrade Requested",
          "format" : "filtered_html",
          "safe_value" : "\\u003Cp\\u003EUpgrade Requested\\u003C\\/p\\u003E\\n"
        },
        {
          "value" : "An upgrade has been requested by:\\r\\nUser Name: [message:field-user-ref:field-first-name] [message:field-user-ref:field-last-name]\\r\\nUser Number: \\u003Ca href =\\u0022[message:field-user-ref:edit-url]\\u0022\\u003E[message:field-user-ref:uid]\\u003C\\/a\\u003E\\r\\nEmail Address: [message:field-user-ref:mail]",
          "format" : "full_html",
          "safe_value" : "\\u003Cp\\u003EAn upgrade has been requested by:\\u003Cbr \\/\\u003E\\nUser Name: [message:field-user-ref:field-first-name] [message:field-user-ref:field-last-name]\\u003Cbr \\/\\u003E\\nUser Number: \\u003Ca href=\\u0022[message:field-user-ref:edit-url]\\u0022\\u003E[message:field-user-ref:uid]\\u003C\\/a\\u003E\\u003Cbr \\/\\u003E\\nEmail Address: [message:field-user-ref:mail]\\u003C\\/p\\u003E\\n"
        }
      ]
    },
    "rdf_mapping" : []
  }');
  return $items;
}
