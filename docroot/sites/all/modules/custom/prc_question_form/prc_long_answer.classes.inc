<?php

/**
 * Extension of MultichoiceQuestion.
 */
class PRCLongAnswerQuestion extends LongAnswerQuestion {

  /**
   * Implementation of delete
   *
   * @see QuizQuestion#delete($only_this_version)
   */
  public function delete($only_this_version = FALSE) {
    $delete_node = db_delete('quiz_short_answer_node_properties');
    $delete_node->condition('nid', $this->node->nid);

    if ($only_this_version) {
      $delete_node->condition('vid', $this->node->vid);
    }

    $delete_node->execute();
  }

  public function saveNodeProperties($is_new = FALSE) {
    if (!isset($this->node->feedback)) {
      $this->node->feedback = '';
    }
    // Leave rubric empty since we aren't currently using it
    if ($is_new || $this->node->revision == 1) {

      $id = db_insert('quiz_long_answer_node_properties')
        ->fields(array(
          'nid' => $this->node->nid,
          'vid' => $this->node->vid,
          'rubric' => ''
        ))
        ->execute();
    }
    else {
      // Leave rubric empty since we aren't currently using it
      db_update('quiz_long_answer_node_properties')
        ->fields(array('rubric' => ''))
        ->condition('nid', $this->node->nid)
        ->condition('vid', $this->node->vid)
        ->execute();
    }
  }

}