
//

Drupal.behaviors.prc_adp = {
  attach : function(context, settings){
    (function ($) {
      $('.prc-adp-launch-test-link').click(
        function(event){
          event.preventDefault();

          var form = $(event.target).closest('form');
          var path = form.find('a.prc-adp-launch-test-link').attr('href');

          // Send a google page view event to register the clicking of the link
          ga('send', 'pageview', path);

          form.submit();
        });
    }(jQuery));
  }
}