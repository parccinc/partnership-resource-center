
<form id='prc-adp-launch-form' action='<?php echo $full_path ?>' method='post' target="_blank">
  <input type='hidden' name='testKey' value='<?php echo $test_key ?>' />
  <input type='hidden' name='name' value='' />
</form>


<script type="application/javascript">
  jQuery(document).ready(
    function(){
      // Delay redirect to allow google analytics to run
      window.setTimeout(
        function(){
          jQuery("#prc-adp-launch-form").submit();
          console.log('form submitted' + Date.now());
        }, 500
      )
    }
  );
</script>