<?php

/**
 * @file
 * PRC TRT School Uploads page callbacks.
 */

/**
 * Page callback for password reset request confirmation page.
 */
function prc_sso_user_reset_requested_page() {

  return array('#markup' => '<p>' . t('An email with a link to reset your password has been sent to the email address you entered. The link will expire after 24 hours, and nothing will happen if the link is not used.') . '</p>');

}
