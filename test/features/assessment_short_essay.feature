@api @assessment
Feature: Adding short essay items.

  @javascript
  Scenario: PRC-2086 - Scenario 1 - Links are present and in the right order
    Given I am logged in as a user with the "Educator" role
    And "Subject" terms:
      | name  |
      | subj1 |
      | subj2 |
    And "Grade Level" terms:
      | name      |
      | Grade 547 |
    And "Assessment" nodes:
      | title        | field_subject | field_grade_level | field_quiz_type   | uid         |
      | PRC-547 View | subj1, subj2  | Grade 547         | Custom Assessment | @currentuid |
    When I visit the last node created
    And I press "Add Item"
    Then I should see the link "Short Essay"
    And "Short Answer" should precede "Short Essay" for the query ".f-dropdown li"

  Scenario: PRC-2086 - Scenario 2 - Form displays correctly
    Given I am logged in as a user with the "Educator" role
    And "Subject" terms:
      | name  |
      | subj1 |
      | subj2 |
    And "Grade Level" terms:
      | name      |
      | Grade 547 |
    And "Assessment" nodes:
      | title        | field_subject | field_grade_level | field_quiz_type   | uid         |
      | PRC-547 View | subj1, subj2  | Grade 547         | Custom Assessment | @currentuid |
    And I am logged in as a user with the "Educator" role
    When I visit "node/add/long-answer"
    Then I should see the text "Create Short Essay Item"

  Scenario: prc-2083 - scenario 1 - Short Essay shows up in context menu on quiz bank
    Given I am logged in as a user with the "Educator" role
    When I visit "assessments/item-bank"
    Then I should see the link "Short essay (manually scored)"

  Scenario: prc-2083 - scenario 2 - Clicking on the link takes us to the form
    Given I am logged in as a user with the "Educator" role
    When I visit "assessments/item-bank"
    And I click "Short essay (manually scored)"
    Then I should see the text "Create Short Essay Item"

  @javascript
  Scenario: prc-2088 - scenario 1 - adding a new item to an existing assessment
    Given I am logged in as a user with the "Educator" role
    And "Subject" terms:
      | name  |
      | subj1 |
      | subj2 |
    And "Grade Level" terms:
      | name      |
      | Grade 547 |
    And "Assessment" nodes:
      | title                 | field_subject | field_grade_level | field_quiz_type   | uid         |
      | @timestamp assessment | subj1, subj2  | Grade 547         | Custom Assessment | @currentuid |
    And I visit the last node created
    And I press "Add Item"
    And I click "Short Essay"
    When I enter "My question @timestamp" for "title"
    And I enter "What is the meaning of life?" for "Question"
    And I check the box "Grade 547"
    Then I press the "Save" button
    And I should see "My question @timestamp" in the "table#question-list" element
    And I should see "@timestamp assessment" in the "main h1" element

  @javascript
  Scenario: prc-2088 - scenario 2 - adding a new item to the item bank
    Given I am logged in as a user with the "Educator" role
    When I visit "assessments/item-bank"
    And I click "Short essay (manually scored)"
    And I fill in "My Title @timestamp" for "Title"
    And I fill in "Who are you?" for "Question"
    And I check the box "1st Grade"
    And I press "Save"
    And I should see "My Title @timestamp" in the ".view-prc-item-bank table.views-table" element
    And I should see "Item Bank" in the "h1" element

  @javascript
  Scenario: prc-2091 - Assessment preview item
    Given I am logged in as a user with the "Educator" role
    And "Subject" terms:
      | name  |
      | subj1 |
      | subj2 |
    And "Grade Level" terms:
      | name      |
      | Grade 547 |
    And "Assessment" nodes:
      | title                 | field_subject | field_grade_level | field_quiz_type   | uid         |
      | @timestamp assessment | subj1, subj2  | Grade 547         | Custom Assessment | @currentuid |
    And I visit the last node created
    And I press "Add Item"
    And I click "Short Essay"
    When I enter "My question @timestamp" for "title"
    And I enter "What is the meaning of life?" for "Question"
    And I check the box "Grade 547"
    Then I press the "Save" button
    And I click "My question @timestamp"
    And I wait for AJAX to finish
    And I should see the text "What is the meaning of life?"
    And I should see the text "Short Essay Item"
    And I should see the text "Enter your answer here"

  @javascript
  Scenario: prc-2092 - Item bank preview item
    Given I am logged in as a user with the "Educator" role
    When I visit "assessments/item-bank"
    And I click "Short essay (manually scored)"
    And I fill in "My Title @timestamp" for "Title"
    And I fill in "Who are you?" for "Question"
    And I check the box "1st Grade"
    And I press "Save"
    And I should see "Item Bank" in the "h1" element
    And I click "My Title @timestamp"
    And I wait for AJAX to finish
    Then I should see the text "Who are you?"
    And I should see the text "Short Essay Item"
    And I should see the text "Enter your answer here"

  @javascript
  Scenario: prc-2089 - Adding an item to an assessment from the bank
    Given I have no "Multiple choice question" nodes
    And I have no "Short answer question" nodes
    And I have no "Assessment directions" nodes
    And I am logged in as a user with the "Educator" role
    And I am on "assessments/item-bank"
    And I click "Short essay (manually scored)"
    And I fill in "Title" with "SE @timestamp"
    And I fill in "Question" with "Why?"
    And I check the box "1st Grade"
    And I press "Save"
    And "Subject" terms:
      | name  |
      | subj1 |
      | subj2 |
    And "Grade Level" terms:
      | name      |
      | Grade 547 |
    And "Assessment" nodes:
      | title                 | field_subject | field_grade_level | field_quiz_type   | uid         |
      | @timestamp assessment | subj1, subj2  | Grade 547         | Custom Assessment | @currentuid |
    And I visit the last node created
    When I press "Add Item"
    And I click "Add Existing Item"
    And I check the box "edit-views-bulk-operations-0"
    And I press "Add questions to Assessment"
    And I follow meta refresh
    Then I should see the message containing "Performed Add questions to Assessment on 1 item."
    When I click "Back to Assessment"
    Then I should see the heading "@timestamp assessment"
    And I should see the link "SE @timestamp"

  @javascript
  Scenario: prc-2087 - Scenario 1 - Create form breadcrumb
    Given I am logged in as a user with the "Educator" role
    When I visit "node/add/long-answer"
    Then I should see "PRC" in the ".breadcrumb a[href='/']" element
    And I should see "Assessment" in the ".breadcrumb a[href='/assessments']" element
    And I should see "Item Bank" in the ".breadcrumb a[href='/assessments/item-bank']" element
    And I should see "Create Short Essay Item" in the ".breadcrumb" element

  @javascript
  Scenario: prc-2087 - Scenario 2 - Edit form breadcrumb
    Given I have no "Multiple choice question" nodes
    And I have no "Short answer question" nodes
    And I have no "Assessment directions" nodes
    And I am logged in as a user with the "Educator" role
    When I visit "node/add/long-answer"
    And I am logged in as a user with the "Educator" role
    When I visit "assessments/item-bank"
    And I click "Short essay (manually scored)"
    And I fill in "My Title @timestamp" for "Title"
    And I fill in "Who are you?" for "Question"
    And I check the box "1st Grade"
    And I press "Save"
    And I click "Edit"
    Then I should see "PRC" in the ".breadcrumb a[href='/']" element
    And I should see "Assessment" in the ".breadcrumb a[href='/assessments/']" element
    And I should see "Item Bank" in the ".breadcrumb a[href='/assessments/item-bank/']" element
    And I should see "My Title @timestamp" in the ".breadcrumb" element
